var app = angular.module("SubjectManagement", []);

// Controller Part
app.controller("SubjectController", function($scope, $http) {
	$scope.listSubjects = [];
	
	$scope.subjectForm = {
		subjectId: -1,
		subjectName: "",
		subjectLimit: 0,
		subjectRegister: 0
	};
	// Clear the form
	function _clearFormData() {
		$scope.subjectForm.subjectId = -1;
		$scope.subjectForm.subjectName = "";
		$scope.subjectForm.subjectLimit = 0;
		$scope.subjectForm.subjectRegister = 0;
	};
	
	// Now load the data from server
	_refreshSubjectData();

	// HTTP POST/PUT methods for add/edit student
	$scope.submitSubject = function() {

		var method = "";
		var url = "";

		if ($scope.subjectForm.subjectId == -1) {
			method = "POST";
			url = '/subject';
		} else {
			method = "PUT";
			url = '/subject';
		}

		$http({
			method: method,
			url: url,
			data: angular.toJson($scope.subjectForm),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(_success, _error);
	};

	$scope.createSubject = function() {
		_clearFormData();
	}

	// HTTP DELETE
	$scope.deleteSubject = function(subject) {
		_refreshStudentData();
		$http({
			method: 'DELETE',
			url: '/subject/' + subject.subjectId
		}).then(function success(response) {
			_refreshStudentData();
			console.log(response);

		}, function error(response) {
			// this function will be called when the request returned error status
		});
	};

	// In case of edit
	$scope.editSubject = function(subject) {
		$scope.subjectForm.subjectId = subject.subjectId;
		$scope.subjectForm.subjectName = subject.subjectName;
		$scope.subjectForm.subjectLimit = subject.subjectLimit;
		$scope.subjectForm.subjectRegister = subject.subjectRegister;
	};

	// Private Method  
	// HTTP GET- get all employees collection
	// Call: http://localhost:8080/student
	function _refreshSubjectData() {
		$http({
			method: 'GET',
			url: '/subjects'
		}).then(
			function(res) { // success
				$scope.listSubjects = res.data;
			},
			function(res) { // error
				console.log("Error: " + res.status + " : " + res.data);
			}
		);
	}

	function _success(res) {
		_refreshSubjectData();
		_clearFormData();
	}

	function _error(res) {
		var data = res.data;
		var status = res.status;
		var header = res.header;
		var config = res.config;
		alert("Error: " + status + ":" + data.toString());
	}

	
});