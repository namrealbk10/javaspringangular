var app = angular.module("StudentManagement", []);

// Controller Part
app.controller("StudentController", function($scope, $http) {
	$scope.listStudents = [];
	
	$scope.studentForm = {
		id: -1,
		fullName: "",
		course: "",
		institute:""
	};
	// Clear the form
	function _clearFormData() {
		$scope.studentForm.id = -1;
		$scope.studentForm.fullName = "";
		$scope.studentForm.course = "";
		$scope.studentForm.institute = "";
	};
	
	// Now load the data from server
	_refreshStudentData();

	// HTTP POST/PUT methods for add/edit student
	$scope.submitStudent = function() {

		var method = "";
		var url = "";

		if ($scope.studentForm.id == -1) {
			method = "POST";
			url = '/student';
		} else {
			method = "PUT";
			url = '/student';
		}

		$http({
			method: method,
			url: url,
			data: angular.toJson($scope.studentForm),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(_success, _error);
	};

	$scope.createStudent = function() {
		_clearFormData();
	}

	// HTTP DELETE
	$scope.deleteStudent = function(student) {
		_refreshStudentData();
		$http({
			method: 'DELETE',
			url: '/student/' + student.id
		}).then(function success(response) {
			_refreshStudentData();
			console.log(response);

		}, function error(response) {
			// this function will be called when the request returned error status
		});
	};

	// In case of edit
	$scope.editStudent = function(student) {
		$scope.studentForm.id = student.id;
		$scope.studentForm.fullName = student.fullName;
		$scope.studentForm.course = student.course;
		$scope.studentForm.institute = student.institute;
	};

	// Private Method  
	// HTTP GET- get all employees collection
	// Call: http://localhost:8080/student
	function _refreshStudentData() {
		$http({
			method: 'GET',
			url: '/students'
		}).then(
			function(res) { // success
				$scope.listStudents = res.data;
			},
			function(res) { // error
				console.log("Error: " + res.status + " : " + res.data);
			}
		);
	}

	function _success(res) {
		_refreshStudentData();
		_clearFormData();
	}

	function _error(res) {
		var data = res.data;
		var status = res.status;
		var header = res.header;
		var config = res.config;
		alert("Error: " + status + ":" + data);
	}

	
});