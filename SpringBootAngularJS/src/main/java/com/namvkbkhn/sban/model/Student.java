package com.namvkbkhn.sban.model;

public class Student {
	private long id;
	private String fullName;
	private String course;
	private String institute;
	
	public Student() {
		super();
	}
	
	public Student(long id, String fullName, String course, String institute) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.course = course;
		this.institute = institute;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	
	public String getInstitute() {
		return institute;
	}
	public void setInstitute(String institute) {
		this.institute = institute;
	}
}
