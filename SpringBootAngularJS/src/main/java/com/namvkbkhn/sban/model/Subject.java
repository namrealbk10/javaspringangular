package com.namvkbkhn.sban.model;

public class Subject {
	private int subjectId;
	private String subjectName;
	private int subjectLimit;
	private int subjectRegister;
	
	public Subject(int subjectId, String subjectName, int subjectLimit, int subjectRegister) {
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.subjectLimit = subjectLimit;
		this.subjectRegister = subjectRegister;
	}
	
	public int getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	public int getSubjectLimit() {
		return subjectLimit;
	}
	public void setSubjectLimit(int subjectLimit) {
		this.subjectLimit = subjectLimit;
	}
	
	public int getSubjectRegister() {
		return subjectRegister;
	}
	public void setSubjectRegister(int subjectRegister) {
		this.subjectRegister = subjectRegister;
	}
}
