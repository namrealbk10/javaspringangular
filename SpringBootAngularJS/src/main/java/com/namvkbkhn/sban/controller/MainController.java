package com.namvkbkhn.sban.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
 
@Controller
public class MainController {
	
    
    @RequestMapping("/managestudent")
    public String manageStudent() {
        return "ManageStudent";
    }
    
    @RequestMapping("/managesubject")
    public String manageSubject() {
        return "ManageSubject";
    }
   
}