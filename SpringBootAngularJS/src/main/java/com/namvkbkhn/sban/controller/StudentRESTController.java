package com.namvkbkhn.sban.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.namvkbkhn.sban.dao.StudentDAO;

import com.namvkbkhn.sban.model.Student;

@RestController
public class StudentRESTController {
	    
	    //////////////////////
	    @Autowired
		private StudentDAO studentDAO;

		@RequestMapping(value = "/students", //
				method = RequestMethod.GET, //
				produces = { MediaType.APPLICATION_JSON_VALUE, //
						MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public List<Student> getStudent() {
			List<Student> list = studentDAO.getListStudents();
			return list;
		}
	    
		@RequestMapping(value = "/student", //
				method = RequestMethod.PUT, //
				produces = { MediaType.APPLICATION_JSON_VALUE, //
						MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public Student updateStudent(@RequestBody Student stuForm) {
			return studentDAO.updateStudent(stuForm);
		}
		
		@RequestMapping(value = "/student", //
				method = RequestMethod.POST, //
				produces = { MediaType.APPLICATION_JSON_VALUE, //
						MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public Student createStudent(@RequestBody Student stuForm) {
			return studentDAO.createStudent(stuForm);
		}
		
		@RequestMapping(value = "/student/{stuId}", //
				method = RequestMethod.DELETE, //
				produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public void deleteStudent(@PathVariable("stuId") Long stuId) {
			studentDAO.deleteStudent(stuId);
		}
	

}
