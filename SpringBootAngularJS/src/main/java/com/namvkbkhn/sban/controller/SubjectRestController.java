package com.namvkbkhn.sban.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.namvkbkhn.sban.dao.SubjectDAO;
import com.namvkbkhn.sban.model.Subject;



@RestController
public class SubjectRestController {
	    
	    //////////////////////
	    @Autowired
		private SubjectDAO subjectDAO;

		@RequestMapping(value = "/subjects", //
				method = RequestMethod.GET, //
				produces = { MediaType.APPLICATION_JSON_VALUE, //
						MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public List<Subject> getSubject() {
			List<Subject> list = subjectDAO.getListSubjects();
			return list;
		}
	    
		@RequestMapping(value = "/subject", //
				method = RequestMethod.PUT, //
				produces = { MediaType.APPLICATION_JSON_VALUE, //
						MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public Subject updateSubject(@RequestBody Subject subForm) {
			return subjectDAO.updateSubject(subForm);
		}
		
		@RequestMapping(value = "/subject", //
				method = RequestMethod.POST, //
				produces = { MediaType.APPLICATION_JSON_VALUE, //
						MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public Subject createSubject(@RequestBody Subject subForm) {
			return subjectDAO.createSubject(subForm);
		}
		
		@RequestMapping(value = "/subject/{subId}", //
				method = RequestMethod.DELETE, //
				produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		@ResponseBody
		public void deleteSubject(@PathVariable("subId") int subId) {
			subjectDAO.deleteSubject(subId);
		}
	

}
