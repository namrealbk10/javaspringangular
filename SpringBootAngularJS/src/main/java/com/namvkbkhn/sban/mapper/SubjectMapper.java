package com.namvkbkhn.sban.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.namvkbkhn.sban.model.Subject;

public class SubjectMapper implements RowMapper<Subject>{

	public static final String BASE_SQL 
	= "Select sub.subjectId, sub.subjectName, sub.subjectLimit, sub.subjectRegistered From Subjects sub ";
	@Override
	public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
		int subjectId  = rs.getInt("subjectId");
		String subjectName = rs.getString("subjectName");
		int subjectLimit = rs.getInt("subjectLimit");
		int subjectRegister = rs.getInt("subjectRegistered");
		
		return new Subject(subjectId, subjectName, subjectLimit, subjectRegister);
	}

}
