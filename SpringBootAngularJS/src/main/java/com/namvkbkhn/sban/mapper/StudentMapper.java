package com.namvkbkhn.sban.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.namvkbkhn.sban.model.Student;

public class StudentMapper implements RowMapper<Student> {
	
	public static final String BASE_SQL 
		= "Select stu.Id, stu.Full_Name, stu.course, stu.institute From Students stu ";

	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		long id = rs.getLong("id");
		String fullName = rs.getString("Full_Name");
		String course = rs.getString("course");
		String institute = rs.getString("institute");
		
		return new Student(id, fullName, course, institute);
	}

}
