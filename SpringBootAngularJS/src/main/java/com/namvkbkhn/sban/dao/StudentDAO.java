package com.namvkbkhn.sban.dao;

import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.namvkbkhn.sban.mapper.StudentMapper;
import com.namvkbkhn.sban.model.Student;

@Repository
@Transactional
public class StudentDAO  extends JdbcDaoSupport{

	@Autowired
	public StudentDAO(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	public List<Student> getListStudents() {
		// Select stu.Id, stu.Full_Name, stu.course, stu.institute From Students stu;
		String sql = StudentMapper.BASE_SQL;
		Object[] params = new Object[] {};
		StudentMapper mapper = new StudentMapper();
		List<Student> list = this.getJdbcTemplate().query(sql, params, mapper);
		return list;
	}
	
	public Student updateStudent(Student stuForm) {
		String sqlUpdate = "Update Students set Full_Name = ?,  course =?, institute = ? where Id = ?";
		this.getJdbcTemplate().update(sqlUpdate,
				stuForm.getFullName(), stuForm.getCourse(), stuForm.getInstitute(),
				stuForm.getId());
		return stuForm;
		
	}
	
	public Student createStudent(Student stuForm) {
		String sqlInsert = "Insert into Students( Full_Name, course, institute) values ( ?, ?, ?)";
		this.getJdbcTemplate().update(sqlInsert,
				 stuForm.getFullName(), stuForm.getCourse(), stuForm.getInstitute());
		return stuForm;
		
	}
	
	public void deleteStudent(long id) {
		String sqlDelete = "DELETE FROM  Students where Id = ?";
		this.getJdbcTemplate().update(sqlDelete, id);
		
	}
}
