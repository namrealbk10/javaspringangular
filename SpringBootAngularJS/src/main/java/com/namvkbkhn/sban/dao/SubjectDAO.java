package com.namvkbkhn.sban.dao;

import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.namvkbkhn.sban.mapper.SubjectMapper;
import com.namvkbkhn.sban.model.Subject;


@Repository
@Transactional
public class SubjectDAO extends JdbcDaoSupport{

	@Autowired
	public SubjectDAO(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	public List<Subject> getListSubjects() {
		// Select sub.subjectId, sub.subjectName, sub.subjectLimit, sub.subjectRegistered From Subjects sub 
		String sql = SubjectMapper.BASE_SQL;
		Object[] params = new Object[] {};
		SubjectMapper mapper = new SubjectMapper();
		List<Subject> list = this.getJdbcTemplate().query(sql, params, mapper);
		return list;
	}
	
	public Subject updateSubject(Subject subForm) {
		String sqlUpdate = "Update Subjects set subjectName = ?,  subjectLimit =?, subjectRegistered = ? where subjectId = ?";
		this.getJdbcTemplate().update(sqlUpdate,
				subForm.getSubjectName(), subForm.getSubjectLimit(), subForm.getSubjectRegister(),
				subForm.getSubjectId());
		return subForm; 
		
	}
	
	public Subject createSubject(Subject subForm) {
		String sqlInsert = "Insert into Subjects( subjectName, subjectLimit, subjectRegistered) values ( ?, ?, ?)";
		this.getJdbcTemplate().update(sqlInsert,
				subForm.getSubjectName(), subForm.getSubjectLimit(), subForm.getSubjectRegister());
		return subForm;
		
	}
	
	public void deleteSubject(int subId) {
		String sqlDelete = "DELETE FROM  Subjects where subjectId = ?";
		this.getJdbcTemplate().update(sqlDelete, subId);
		
	}
	
}
